<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;

/**
 * Description of AlumnoModel
 *
 * @author jose
 */
class GrupoModel extends Model{ 
    protected $table = 'tipohabitacion';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    protected $allowedFields = ['id','nombre','descripcion','tecnologia','capacidad','adultos','ninyos'];
}
