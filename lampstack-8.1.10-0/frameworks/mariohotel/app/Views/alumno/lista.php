<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>
descripcion
<?= $this->section('content') ?>descripcion

    <ul class="nav justify-content-end mb-4">
        <li class="nav-item ">
            <a class="nav-link active btn btn-primary" href="<?=site_url('habitaciones/lista')?>">Insertar</a>
        </li>
    </ul>


    <table class="table table-striped" id="hotel">
        <thead>
            <th>
                id
            </th>
            <th>
                Nombre
            </th>
            <th>
                descripcion
            </th>
            <th>
                tecnologia
            </th>
             <th>
                capacidad
            </th>
            <th>
                adultos
            </th>
            <th>
                ninyos
            </th>
        </thead>
        <tbody>
        <?php foreach ($hotel as $hotel): ?>
            <tr>
                <td>
                    <?= $hotel->id ?>
                </td>
                <td>
                    <?= $hotel->nombre ?> 
                </td>
                <td>
                    <?= $hotel->descripcion ?>
                </td>
                <td>
                    <?= $hotel->tecnologia ?>
                </td>                
                <td>
                    <?= $hotel->capacidad ?>
                </td>                
                <td>
                    <?= $hotel->adultos ?>
                </td>                
                <td>
                    <?= $hotel->ninyos ?>
                </td>
                <td class="text-right">
                    <a href="<?=site_url('habitaciones/lista'.$data->id)?>" title="Editar <?= $hotel->id.' '.$hotel->nombre.' '.$hotel->descripcion.' '.$hotel->tecnologia.' '.$hotel->capacidad.' '.$hotel->adultos.' '.$hotel->ninyos ?>">
                    <span class="bi bi-pencil-square"></span>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>    
    </table>
<?= $this->endSection() ?>

