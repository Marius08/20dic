
<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <?= form_open('grupo/actualiza/' . $hotel->id) ?>
    <div class="form-group row">
        <?= form_label('Código:', 'codigo', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('codigo', $hotel->nombre, ['class' => 'form_control', 'id' => 'codigo']) ?>
        </div>
        <?= form_open('grupo/actualiza/' . $hotel->descripcion) ?>
    <div class="form-group row">
        <?= form_label('Código:', 'codigo', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('codigo', $hotel->tecnologia, ['class' => 'form_control', 'id' => 'codigo']) ?>
        </div>
    </div>
    <div class="form-group row">
        <?= form_label('Nombre:', 'nombre', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('nombre', $hotel->capacidad, ['class' => 'form_control', 'id' => 'nombre']) ?>
        </div>
    </div>
        <div class="form-group row">
        <?= form_label('Nombre:', 'nombre', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('nombre', $hotel->adultos, ['class' => 'form_control', 'id' => 'nombre']) ?>
        </div>
    </div>
        <div class="form-group row">
        <?= form_label('Nombre:', 'nombre', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('nombre', $hotel->ninyos, ['class' => 'form_control', 'id' => 'nombre']) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-10">
            <?= form_submit('botoncito', 'Enviar', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= form_close() ?>
<?= $this->endSection() ?>
