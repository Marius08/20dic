<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <table class="table table-striped" id="myTable">
        <thead>
            <tr>
                <th>
                    id
                </th>
                <th>
                    nombre
                </th>
                <th>
                    descripcion
                </th>                
                <th>
                    capacidad
                </th>
                <th>
                    adultos
                </th>
                <th>
                    ninyos
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($hotel as $hotel): ?>
                <tr>
                    <td>
                        <?= $hotel->id ?>
                    </td>
                    <td>
                        <?= $hotel->nombre ?>
                    </td>
                    <td>
                        <?= $hotel->descripcion ?>
                    </td>
                    <td>
                        <?= $hotel->tecnologia ?>
                    </td>
                    <td>
                        <?= $hotel->capacidad ?>
                    </td>
                    <td>
                        <?= $hotel->adultos ?>
                    </td>
                    <td>
                        <?= $hotel->ninyos ?>
                    </td>
                    <td class="text-right">
                        <a href="<?=site_url('grupo/editar/'.$hotel->id)?>">
                            <span class="bi bi-pencil-square" title="Editar el grupo"></span>
                        </a>
                         <a href="<?=site_url('grupo/borrar/'.$hotel->id)?>" onclick="return confirm('¿Deseas borrar el grupo <?=$hotel->nombre?>?')">
                            <span class="bi bi-eraser-fill" title="Editar el grupo"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?= $this->endSection() ?>


