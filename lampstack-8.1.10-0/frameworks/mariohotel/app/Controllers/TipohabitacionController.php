<?php

namespace App\Controllers;
use App\Models\AlumnoModel; 

use Config\Services;


class TipohabitacionController extends BaseController {

    public function index(){
        $TipohabitacionModel = new TipohabitacionModel();
        $hotel['title'] = 'Listado de Habitaciones';
        $hotel['habitaciones'] = $TipohabitacionModel->findAll();
        return view('habitaciones/lista',$hotel);
    }
    
    
        public function borrar($hotel){
        if ($this->auth->loggedIn() AND $this->auth->isAdmin()){
            $TipohabitacionModel = new TipohabitacionModel();
            $TipohabitacionModel->delete($hotel);
            return redirect()->to('TipohabitacionController');
        } else {
            echo "no tiene permiso";
        }
    }

        public function formEdit($hotel){
        helper('form');
        $TipohabitacionModel = new GrupoModel();
        $hotel['title'] = 'Hotel';
        $hotel['hotel'] = $TipohabitacionModel->find($id);
        return view('hotel/formEdit',$hotel);
        
        }
        
        
            public function actualiza($hotel){
        $hotel = $this->request->getPost();
        /*unset($alumno['botoncito']);
        echo '<pre>';
        print_r($alumno);
        echo '</pre>';*/
        $TipohabitacionModel = new TipohabitacionModel();
        $TipohabitacionModel->update($id, $hotel);
        return redirect()->to('habitaciones/lista');
    }

}
