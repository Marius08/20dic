<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function listatablas(){
        $data['title'] = 'Listado de Tablas';
        $grupos = new \App\Models\GrupoModel();
        $data['resultado'] = $grupos ->findAll(); 
     return view('grupos/lista',$data);
   

    }
     public function listaproductos(){
        $data['title'] = 'Listado de Productos';
        $grupos = new GrupoModel();
        $data['resultado'] = $grupos ->findAll(); 
     return view('grupos/lista',$data);
 }


}